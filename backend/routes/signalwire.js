const express = require('express');
const router = express.Router();
const axios = require('axios');

// Auth constants to be stored in a dotenv file (or equivalent) with gitignore
const auth = {
    username: "vxpusername", // Project ID
    password: "vxppassword", // API token
};
  
const apiurl = "https://vxp-1.signalwire.com/api/relay/rest/jwt";

// Endpoint to request Video Room Token for video call
router.post("/get_token", async (req, res) => {
  let { user_name, room_name } = req.body;
  
  try {
    // get the Video Room Token from SignalWire
    const reqstring = `${auth.username}:${auth.password}`;
    let token = await axios.post(
      apiurl,
      {
        reqstring
      },
      { auth }
    );
    console.log(token.data.jwt_token);
    console.log(token.data.refresh_token);

    // send the Video Room Token back to the client
    return res.json({ token: token.data.jwt_token, refresh_token: token.data.refresh_token });
  } catch (e) {
    console.log(e);
    return res.sendStatus(500);
  }
  });

module.exports = router;


