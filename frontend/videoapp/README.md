# Getting Started

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### Documentation

[Frontend Setup: API in React Native](https://developer.signalwire.com/guides/video/getting-started-video-api-react-native/)

[Backend Setup: Generate new Video Room Token](https://developer.signalwire.com/rest/generate-a-new-video-room-token/) see also [Backend Setup: Generate new JWT Token](https://docs.signalwire.com/reference/relay-sdk-react-native/v1/#relay-sdk-for-react-native-using-the-sdk) to use React Native

[Backend Setup: Video API Example](https://developer.signalwire.com/guides/getting-started-with-the-signalwire-video-api-1/)

[Signalwire react-native github](https://github.com/signalwire/signalwire-node/tree/master/packages/react-native)



