import { useCallback, useState, useEffect } from 'react';
import axios from 'axios';
import {
  VideoConference,
  useMembers,
  useStatus,
  useLayouts,
  useScreenShare,
} from '@signalwire-community/react';

export default function DemoVideo() {
  const [roomSession, setRoomSession] = useState(null);
  const onRoomReady = useCallback((rs) => setRoomSession(rs), []);

  const { self, members } = useMembers(roomSession);
  const { active } = useStatus(roomSession);
  const { layouts, setLayout, current_layout } = useLayouts(roomSession);
  const { toggle, active: screenShareActive } = useScreenShare(roomSession);

  const [token, setToken] = useState(null);

  useEffect(() => {
    const userObject = {
      room_name: "Business_Lounge",
      user_name: "B. Rowe"
    };
    const fetchData = async () => {
      const response = await axios.post('http://localhost:5000/api/signalwire/get_token', userObject);
      setToken(response.data.token);
    };

    fetchData();
  }, []);

  return (
    <div style={{ maxWidth: 1920 }}>
      {token && (
        <VideoConference
          token={token}
          onRoomReady={onRoomReady}
        />
      )}

      {/* Populating controls for self */}
      {['audio', 'video', 'speaker'].map((io) => (
        <button onClick={() => self?.[io].toggle()} disabled={!active}>
          {self?.[io].muted ? 'Unmute ' : 'Mute '} {io}
        </button>
      ))}
      <button disabled={!active} onClick={toggle}>
        {screenShareActive ? 'Stop screen share' : 'Start screen share'}
      </button>
      <button onClick={() => self?.remove()} disabled={!active}>
        Leave
      </button>

      {/* Creating a Layout Selector */}
      {active && (
        <select
          value={current_layout}
          onChange={(e) => {
            setLayout({ name: e.target.value });
          }}
        >
          {layouts.map((l) => (
            <option value={l} key={l}>
              {l}
            </option>
          ))}
        </select>
      )}

      {/* Populating members and their controls */}
      <div>
        <b>Members: </b>
        <ul>
          {members.map((member) => (
            <li>
              {member.name}
              {member.talking && '🗣'}

              {['audio', 'video', 'speaker'].map((io) => (
                <button onClick={member[io].toggle}>
                  {member[io].muted ? 'Unmute ' : 'Mute '} {io}
                </button>
              ))}
              <button onClick={member.remove}>Remove</button>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
}