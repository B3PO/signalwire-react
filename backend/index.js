const express = require('express');
const path = require('path');
const cors = require('cors');

const app = express();
const PORT = process.env.PORT || 5000;

var corsOptions = {
    origin: "*"
};

app.use(cors(corsOptions));  
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname, 'public')))

app.use('/api/signalwire', require('./routes/signalwire'));

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});


