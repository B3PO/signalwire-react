# signalwire-react

## Getting started
```
git clone https://gitlab.com/B3PO/signalwire-react.git
```


## Run Backend

```
cd signalwire-react/backend
npm i
npm run dev
```

## Run Frontend

```
cd signalwire-react/frontend/videoapp
npm i
npm run start
```




